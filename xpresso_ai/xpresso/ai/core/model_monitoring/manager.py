"""
    Manager class for Model Monitoring
"""

__all__ = ["ModelMonitoringManager"]
__author__ = ["Shlok Chaudhari"]


from xpresso.ai.core.commons.utils.constants import SERVER_URL_FIELD, RESULTS_KEY, \
    MODEL_TYPE_KEY, MONITOR_SETTINGS_KEY, ENABLE_CALIBRATION_KEY, MODEL_DESCRIPTION_KEY, \
    MODEL_ID, TOKEN, MM_PROJECT_NAME_KEY, MODEL_NAME_KEY, MODEL_VERSION_KEY, RECALIBRATE_KEY, \
    UNDEFINED_MODEL_TYPE


class ModelMonitoringManager:
    """
        This is a manager class for Model Monitoring. Here
        we define all the methods & variables required for
        integrating Model Monitoring with xpresso platform
    """

    def __init__(self, request_client, mm_config, token):
        self._token = token
        self._mm_config = mm_config
        self._base_url = self._mm_config[SERVER_URL_FIELD]
        self._request_client = request_client

    @property
    def get_model_id_route(self): return f"{self._base_url}/model/id"

    def get_model_id(self, project_name: str, model_name: str,
                     model_version: int, verify: bool = False):
        """
            Get Model ID for mentioned model
        Args:
            project_name(str): name of project
            model_name(str): name of deployed model
            model_version(int): version of deployed model
            verify(bool): SSL certificate verification
        Returns:
            response(dict)
        Raises:
            ModelMonitoringServiceFailed
        """
        data = {
            MM_PROJECT_NAME_KEY: project_name,
            MODEL_NAME_KEY: model_name,
            MODEL_VERSION_KEY: model_version
        }
        response = self._request_client.send(
            "GET", self.get_model_id_route, json=data,
            headers={TOKEN: self._token}, verify=verify)
        return response.json()[RESULTS_KEY][MODEL_ID]

    @property
    def manage_models_route(self): return f"{self._base_url}/model/monitor"

    def create_new_model(self, project_name: str, model_name: str,
                         model_version: int, files=None, verify: bool = False):
        """
            Create a new model entry in Model Monitoring
        Args:
            project_name(str): name of project
            model_name(str): name of deployed model
            model_version(int): version of deployed model
            files(list): files to be sent in the request
            verify(bool): SSL certificate verification
        Returns:
            response(dict)
        Raises:
            ModelMonitoringServiceFailed
        """
        files = [] if not files else files
        payload = '{' \
                  f'"{MM_PROJECT_NAME_KEY}": "{project_name}",' \
                  f'"{MODEL_NAME_KEY}": "{model_name}",' \
                  f'"{MODEL_VERSION_KEY}": {model_version},' \
                  f'"{MODEL_DESCRIPTION_KEY}": "Model created by xpresso platform",' \
                  f'"{MODEL_TYPE_KEY}": "{UNDEFINED_MODEL_TYPE}",' \
                  f'"{MONITOR_SETTINGS_KEY}":' \
                  '{' \
                  f'"{ENABLE_CALIBRATION_KEY}": false,' \
                  f'"{RECALIBRATE_KEY}": false' \
                  '}' \
                  '}'
        response = self._request_client.send(
            "POST", self.manage_models_route, data={"create_model": payload},
            headers={TOKEN: self._token}, files=files, verify=verify)
        return response.json()[RESULTS_KEY]

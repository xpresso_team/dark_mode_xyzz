import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.utils.multiclass import type_of_target

from xpresso.ai.core.commons.utils.constants import EMPTY_STRING
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.ml_utils.generic import CLASSIFIER, REGRESSOR, \
    CLASSIFIER_TARGET_TYPES, BINARY
from xpresso.ai.core.ml_utils.sklearn import SklearnMetrics
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

MOUNT_PATH = os.environ.get("XPRESSO_MOUNT_PATH", EMPTY_STRING)
METRICS_FOLDER = os.path.join(MOUNT_PATH, "reported_data/metrics")
PLOTS_FOLDER = os.path.join(MOUNT_PATH, "reported_data/plots")


class XgboostPlots:
    def __init__(self):
        self.logger = XprLogger(name="XgboostPlots")
        pass

    def plot_learning_curve(self, estimator=None, evals_result=None):
        """
        Generate learning curves for a trained XGBoost model: the test,
        train scores vs num_boost_round.

        Users can choose to pass in either a trained XGBoost model using
        Scikit-Learn API (xgboost.XGBRegressor or
        xgboost.XGBClassifier) or a dictionary containing evaluation results
        that is returned by training a XBGoost
        Booster model (using XGBoost Learning API xgboost.train and parameter
        evals_result is specified)
        Parameters
        ----------
        estimator : a trained XGBoost model using Scikit-Learn API (
        xgboost.XGBRegressor or xgboost.XGBClassifier)

        evals_result : a dictionary containing evaluation results that is
        returned by training a XBGoost Booster model
                     (using XGBoost Learning API xgboost.train and parameter
                     evals_result is specified)

        """
        evals_result_dict = dict()
        # get evaluation results
        if (estimator is not None) and (
                isinstance(estimator, xgb.sklearn.XGBModel)):
            evals_result_dict = estimator.evals_result()
        if (evals_result is not None) and (isinstance(evals_result, dict)):
            evals_result_dict = evals_result

        # identify unique evaluation metrics used when fitting the xgboost model
        metrics_list = []
        if evals_result_dict.values():
            validation_list = list(evals_result_dict.values())
            metrics_list = list(validation_list[0].keys())
        metrics_set = np.unique(metrics_list)

        # create subplots
        fig, axes = plt.subplots(len(metrics_set), 1, figsize=(8, 12))

        # plot learning curve for each scoring metrics
        for index, metric_name in enumerate(metrics_set):
            try:
                result_dict = {f"{name}_{metric_name}": metrics[metric_name]
                               for name, metrics in evals_result_dict.items()
                               if metric_name in metrics}
                results = pd.DataFrame(result_dict)

                # collect number of boost run
                num_boost_range = list(range(1, results.shape[0] + 1))

                # identify best xgboost run number and the score
                if ("loss" in metric_name) or ("error" in metric_name):
                    val_score_best = results.iloc[:, 1].min()
                    max_iters_best = results.iloc[:, 1].idxmin() + 1
                else:
                    val_score_best = results.iloc[:, 1].max()
                    max_iters_best = results.iloc[:, 1].idxmax() + 1

                # Plot score vs max_iters
                axes[index].grid()
                axes[index].plot(num_boost_range, results.iloc[:, 0],
                                 color="b", label=results.columns[0])
                axes[index].plot(num_boost_range, results.iloc[:, 1],
                                 color="g", label=results.columns[1])
                axes[index].plot(max_iters_best, val_score_best, 'r*',
                                 label='Best num_iteration : ' + str(
                                     max_iters_best))
                axes[index].set_xlabel("num_iteration")
                axes[index].set_ylabel(metric_name)
                learning_curve_title = "XGboost Learning Curve :" + metric_name
                axes[index].set_title(learning_curve_title)
                axes[index].legend(loc="best")
            except Exception:
                self.logger.exception("Unable to plot learning curve for "
                                      "xgboost model")
        fig.tight_layout(pad=3.0)
        xpresso_save_plot("XGboost_learning_curve", output_path=PLOTS_FOLDER)


class XgboostMetrics(SklearnMetrics):
    def __init__(self, model, parameters_json=None):
        if parameters_json is None:
            parameters_json = dict()
        super().__init__(model, parameters_json)
        self.best_ntree_limit = None

    def validate_model_type(self):
        if not (isinstance(self.model, xgb.Booster)
                or isinstance(self.model, xgb.sklearn.XGBModel)):
            return False
        return True

    def report_training_best_iteration(self):
        """
        Identify and report best iteration and best score during XGBoost
        model training
        """
        try:
            self.best_ntree_limit = self.model.best_ntree_limit
        except Exception as exp:
            self.logger.exception(f"Unable to get best iteration metric. "
                                  f"Reason:{exp}")
            self.best_ntree_limit = None

        # datatype check : if model is xgboost model trained with xgboost
        # internal learning API
        if isinstance(self.model, xgb.Booster):
            for k, v in self.model.attributes().items():
                if k == 'best_msg':
                    for metric_pair in v.split()[1:]:
                        metric_name, metric_v = metric_pair.split(":")
                        self.calculated_metrics[f"Best Score ({metric_name}) "
                                                f"During Training"] = metric_v
                else:
                    self.calculated_metrics[k] = v
                    # datatype check : if model is xgboost model trained with
        # Scikit-Learn API
        elif isinstance(self.model, xgb.sklearn.XGBModel):
            try:
                self.calculated_metrics[
                    "Best Score During Training"] = self.model.best_score
                self.calculated_metrics[
                    "Best Iteration During Training"] = \
                    self.model.best_iteration
            except Exception as exp:
                self.logger.exception(f"Unable to get best iteration metric. "
                                      f"Reason:{exp}")

    def set_model_type(self, y_values, model_type=EMPTY_STRING):
        """
                Determine the model is a regressor or classifier.
                If user provides True/False to is_regressor parameter,
                apply user's
                input. If not, infer model type
                based on the data type of target variable with Sklearn
                type_of_target
                method
                """
        xgb_objectives_clf_binary = ["binary:logistic", "binary:logitraw",
                                     "binary:hinge"]
        xgb_objectives_clf_multicls = ["multi:softmax", "multi:softprob"]
        xgb_objectives_regr = ["reg:squarederror", "reg:squaredlogerror",
                               "reg:logistic",
                               "reg:pseudohubererror", "reg:gamma",
                               "reg:tweedie"]
        xgb_eval_metrics_clf = ["logloss", "error", "error@t", "merror",
                                "mlogloss", "auc", "aucpr"]
        xgb_eval_metrics_regr = ["rmse", "rmsle", "mae", "mphe",
                                 "poisson-nloglik",
                                 "gamma-nloglik", "cox-nloglik",
                                 "gamma-deviance", "tweedie-nloglik"]
        if model_type and model_type.lower() in [REGRESSOR, CLASSIFIER]:
            self.model_type = model_type
        elif isinstance(self.model, xgb.XGBRegressor):
            self.model_type = REGRESSOR
        elif isinstance(self.model, xgb.XGBClassifier):
            self.model_type = CLASSIFIER
        # if user doesn't specify model type, try identify model type from
        # model objective or evaluation metrics
        # from the parameters of model
        else:
            params_metrics, params_objective = \
                self.identify_params_obj_metrics()
            # first try identifying model type from model objective
            if params_objective:
                try:
                    if (params_objective in xgb_objectives_clf_binary) or \
                            (params_objective in xgb_objectives_clf_multicls):
                        self.model_type = CLASSIFIER
                    elif params_objective in xgb_objectives_regr:
                        self.model_type = REGRESSOR
                except Exception as exp:
                    self.logger.exception(f"Unable to identify model type. "
                                          f"Reason: {exp}")

            # then try identifying model type from model evaluation metrics
            elif params_metrics:
                try:
                    if any(mtr in xgb_eval_metrics_clf for mtr in
                           params_metrics):
                        self.model_type = CLASSIFIER
                    elif any(mtr in xgb_eval_metrics_regr for mtr in
                             params_metrics):
                        self.model_type = REGRESSOR
                except Exception as exp:
                    self.logger.exception(f"Unable to identify model type. "
                                          f"Reason: {exp}")
        # if user model type not detected from above methods, use sklearn
        # type_of_target method to identify data type of target variable
        if self.model_type not in [REGRESSOR, CLASSIFIER]:
            self.set_target_type(y_values)
            if self.target_type.startswith("continuous"):
                self.model_type = REGRESSOR
            elif self.target_type in CLASSIFIER_TARGET_TYPES:
                self.model_type = CLASSIFIER

    def identify_params_obj_metrics(self):
        """
        Identify objectives and evaluation metrics from the model parameters
        """
        params_objective = None
        params_metrics = None
        if not isinstance(self.model, xgb.sklearn.XGBModel):
            return params_metrics, params_objective
        try:
            params_objective = self.model.objective
            # collect name of evaluation metrics from model.evals_results
            params_metrics = []
            for val_sets, e_mtrs in self.model.evals_result().items():
                for e_mtr_name, e_mtr_vals in e_mtrs.items():
                    params_metrics.append(e_mtr_name)
        except Exception:
            import traceback
            traceback.print_exc()
        return params_metrics, params_objective

    def predict_and_evaluate(self, x_values, y_values, prediction_threshold,
                             dataset_name=EMPTY_STRING,
                             model_type=EMPTY_STRING):
        """
        Args:
            x_values: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y_values: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set,
                validation set, or test set)
            dataset_name: string
                 The value will be "tr" and "val", indicating training set,
                validation set, if generate_validation_metrics
                is True.
            model_type : String (Optinal) ["regressor", "classifier"] If no
                value is given, the model type will be inferred from model
                loss or the data type of target variable (y) using
                sklearn.utils.multiclass.type_of_target method.
        """
        self.metric_suffix = dataset_name
        # identify model_type
        self.set_model_type(y_values, model_type)
        # identify target_type
        self.set_target_type(y_values)

        self.report_training_best_iteration()

        # datatype check : if model is xgboost model trained with
        # Scikit-Learn API
        if isinstance(self.model, xgb.sklearn.XGBModel):
            xgboost_plot_utils = XgboostPlots()
            xgboost_plot_utils.plot_learning_curve(self.model,
                                                   self.metric_suffix)
        # assign independent variables
        if isinstance(self.model, xgb.Booster):
            x_values = xgb.DMatrix(x_values, label=y_values)
        else:
            x_values = x_values

        # make predictions
        y_pred, y_pred_proba = self.generate_prediction(x_values, y_values)
        y_pred = self.get_y_pred_label(y_pred, prediction_threshold)

        # populate metrics
        self.populate_metrics(x_values, y_values, y_pred, y_pred_proba, None)

    def get_y_pred_label(self, y_pred, prediction_threshold):
        if type_of_target(y_pred).startswith("continuous") or \
                self.target_type.startswith("continuous"):
            y_pred = (y_pred > prediction_threshold).astype(int)
        return y_pred

    def generate_prediction(self, x_values, y_values):
        """
        Generate prediction using model.predict()
        Returns: Prediction, the ooutput from running XGBoost model.predict()
        (labeled as y_pred).
                Additional, predicted probabilities if the classification
                model has attribute of predict_proba.
        """
        y_pred_proba = None
        try:
            # Apply trained model to make predictions on given dataset
            if self.best_ntree_limit:
                y_pred = self.model.predict(x_values,
                                            ntree_limit=self.best_ntree_limit)
            else:
                y_pred = self.model.predict(x_values)
            # Check if the trained model can predict probability
            if hasattr(self.model, "predict_proba"):
                y_pred_proba = self.model.predict_proba(x_values)

            # if objective is binary:logistic, the output of model.predict()
            # is probabilities not the exact label
            if self.target_type in CLASSIFIER_TARGET_TYPES and \
                self.target_type.startswith(
                    "continuous"):
                y_pred_proba = y_pred
            elif self.target_type == BINARY:
                y_pred_proba = np.c_[1 - y_pred, y_pred]
            return y_pred, y_pred_proba

        except Exception as exp:
            self.logger.exception(f"Unable to generate prediction."
                                  f"Reason:{exp}")

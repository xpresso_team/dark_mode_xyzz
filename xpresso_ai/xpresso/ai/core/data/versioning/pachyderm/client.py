import os
from tqdm import tqdm

from grpc._channel import _Rendezvous as PachClientException, \
    _InactiveRpcError as PachExecutionException
from xpresso.ai.core.data.versioning.pachyderm.connector \
    import PachydermConnector
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import PachydermOperationException
from xpresso.ai.core.commons.utils.generic_utils \
    import get_size_in_readable_format, read_file_obj_in_chunks
from xpresso.ai.core.logging.xpr_log import XprLogger

# TODO: Make sure to add exceptions for all the client methods


class PachydermClient:
    """
    Our own wrapper for pachyderm python client


    """
    def __init__(self, host, port):
        """
        constructor class for Pachyderm client

        creates a new PfsClient connection to the pachyderm cluster
        and saves it to the state of this class instance
        """
        connector = PachydermConnector(host, port)
        self.client = connector.connect()
        self.logger = XprLogger()

    def create_new_repo(self, repo_name, description=None, update=None):
        """
        :param repo_name:
            name of the repo
        :param description:
            description on the repo(Optional)
        :param update:
            update flag to overwrite if repo already exists
        :return:
        """
        try:
            self.client.create_repo(repo_name, description, update)
        except PachClientException as err:
            self.logger.error(err.details())
            raise PachydermOperationException()

    def get_repo(self):
        """
        returns the list of all the repos on pachyderm cluster

        :return:
            list of repos
        """
        repos = self.client.list_repo()
        return repos

    def delete_repo(self, repo_name):
        """
        deletes a repo
        :param repo_name:
            name of the repo to be deleted
        :return:
        """
        self.client.delete_repo(repo_name)

    def create_new_branch(self, repo_name, branch_name):
        """
        create a new branch in specified repo

        :param repo_name:
            name of the repo
        :param branch_name:
            name of the branch
        :return:
        """
        try:
            self.client.create_branch(repo_name, branch_name)
        except PachClientException as err:
            self.logger.error(err.details())
            raise PachydermOperationException()

    def get_branch(self, repo_name):
        """
        returns list of branches in a repo
        :param repo_name:
            name of the repo
        """
        branches = self.client.list_branch(repo_name)
        return branches

    def inspect_branch(self, repo_name, branch_name):
        """
        provides information on a branch

        :param repo_name:
            name of the repo where branch resides
        :param branch_name:
            name of the branch
        :return:
            branch information object
        """
        try:
            branch_info = self.client.inspect_branch(repo_name, branch_name)
            return branch_info
        except PachClientException as err:
            self.logger.error(err.details())
            raise PachydermOperationException()

    def delete_branch(self, repo_name, branch_name):
        """
        deletes a branch from the specified repo

        :param repo_name:
            name of the repo branch is in
        :param branch_name:
            name of the branch that needs to be deleted
        """
        self.client.delete_branch(repo_name, branch_name)

    def inspect_commit(self, repo_name, commit_id, block_state=None):
        """
        provides information on a commit

        :param repo_name:
            name of the repo
        :param commit_id:
            commit id
        :param block_state:
        :return:
            returns CommitInfo Object
        """
        try:
            commit_info = self.client.inspect_commit((repo_name, commit_id),
                                                     block_state)
            return commit_info
        except (PachClientException, PachExecutionException) as err:
            self.logger.error(err.details())
            raise PachydermOperationException()

    def list_commit(self, repo_name, upper_commit=None,
                    lower_commit=None, count=None):
        """
        lists commits in a repo

        :param repo_name:
            name of the commit
        :param upper_commit:
            id of the last commit that needs to be shown
        :param lower_commit:
            id of the commit from which list starts
        :param count:
            number of commits to be shown
        :return:
            A in-flight _Rendezvous object
        """
        if upper_commit:
            upper_commit = (repo_name, upper_commit)
        if lower_commit:
            lower_commit = (repo_name, lower_commit)
        commits = self.client.list_commit(repo_name, upper_commit, lower_commit, count)
        if commits.done():
            # Check if this is the right error for this operation
            raise PachydermOperationException()
        return commits

    def delete_commit(self, repo_name, commit_id):
        """
        deletes a commit and its contents

        :param repo_name:
            name of the repo
        :param commit_id:
            id of the commit that needs to be deleted
        """
        try:
            self.client.delete_commit((repo_name, commit_id))
        except (PachClientException, PachExecutionException) as err:
            self.logger.error(err.details())
            raise PachydermOperationException()

    @staticmethod
    def fetch_dataset_info(file_object):
        """
        Fetches filename & path from FileInfo object

        :param file_object:
            FileInfo object containing info for a file
        :return:
            returns a dict with required file info
        """
        info = {}
        path = file_object.file.path
        info["path"] = path
        # finds the index of / from right end
        # If not found i.e. path itself is file_name it returns -1
        # name start index will be right after last '/' or 0
        name_start_index = path.rfind("/") + 1
        info["file_name"] = path[name_start_index:]
        info["type"] = "File" if file_object.file_type == 1 else "Folder"
        info["size_in_bytes"] = file_object.size_bytes
        return info

    def list_dataset(self, repo_name, commit_id, path="/",
                     history=None, include_contents=None):
        """
        list the automl in a branch or provided commit

        :param repo_name:
            name of the repo
        :param commit_id:
            id of the commit
        :param path:
            path to the automl file/folder
        :param history:
            (Optional) retrieves previous versions of file
        :param include_contents:
            includes file contents in response
        :return:

        """
        files = self.client.list_file((repo_name, commit_id), path,
                                      history, include_contents)
        if files.done():
            raise PachydermOperationException()
        file_list = []
        sub_dir_path_list = []
        try:
            for file_item in files:
                file_info = self.fetch_dataset_info(file_item)
                file_list.append(file_info)
                # if file type is a directory
                if file_item.file_type == 2:
                    sub_dir_path_list.append(file_item.file.path)
        except PachClientException as err:
            self.logger.error(err.details())
            raise PachydermOperationException()

        for sub_dir_path in sub_dir_path_list:
            file_list += self.list_dataset(repo_name, commit_id, sub_dir_path,
                                           history, include_contents)

        return file_list

    def push_dataset(self, repo_name, branch_name,
                     file_path_list, push_description=None):
        """
        pushes a automl into pachyderm cluster

        starts a new commit and pushes the file provided at the `path`
        to the cluster

        :param repo_name:
            name of the repo
        :param branch_name:
            name of the branch
        :param file_path_list:
            list of paths of all the files
        :param push_description:
            description for this push
        :return:
            status of the push
        """
        # TODO: add FileNotFound & byte encoding exceptions
        # TODO: Update push_dataset output: Output should also contain pachyderm path
        try:
            # opens a new commit
            new_commit = self.client.start_commit(repo_name,
                                                  branch_name, parent=None,
                                                  description=push_description)
        except PachClientException as err:
            # returns in case of opening a new commit
            self.logger.error(err.details())
            raise PachydermOperationException()

        try:
            list_in_tqdm = tqdm(file_path_list, leave=False)

            for (local_path, pachyderm_path) in list_in_tqdm:
                # fetching file extension before reading
                # file_extension = os.path.splitext(local_path)[1]
                file_size = os.stat(local_path).st_size
                list_in_tqdm.set_description(
                    f"pushing `{local_path}`; Size:{file_size}\n")
                with open(local_path, "rb") as dataset_fs:
                    # Read chunks of size 5 MB at a time
                    is_first_chunk = True
                    chunk_size = 5 * 1024 * 1024
                    for chunk in tqdm(
                        read_file_obj_in_chunks(dataset_fs,
                                                chunk_size=chunk_size),
                        total=int(file_size/chunk_size)+1):
                        if is_first_chunk:
                            self.client.put_file_bytes((repo_name, new_commit.id),
                                                       pachyderm_path, chunk,
                                                       overwrite_index=0)
                            is_first_chunk = False
                        else:
                            self.client.put_file_bytes((repo_name, new_commit.id),
                                                        pachyderm_path, chunk)
            self.client.finish_commit((repo_name, new_commit.id))
            return new_commit.id
        except PachClientException as err:
            # removes the above commit
            self.client.delete_commit((repo_name, new_commit.id))
            self.logger.error(err.details())
            raise PachydermOperationException()
        except Exception:  # This exception is intended. Commit must be deleted.
            self.client.delete_commit((repo_name, new_commit.id))
            raise PachydermOperationException("Pushing dataset failed")

    def pull_dataset(self, repo_name, commit_id, path, file_obj):
        """
        Pulls automl/file at the specified path of the commit

        :param repo_name:
            name of the repo where the automl is saved
        :param commit_id:
            id of the commit when the automl is pushed
        :param path:
            path of the file in the pachyderm cluster
        :param file_obj:
            file object where content is written
        :return:
            returns the fileInfo object if success
        """
        commit_tuple = (repo_name, commit_id)
        file_generator_object = self.client.get_file(commit_tuple, path)
        total_data_size = 0
        try:
            for file_content in tqdm(file_generator_object):
                file_obj.write(file_content)
                total_data_size += len(file_content)
            return total_data_size
        except PachClientException as err:
            self.logger.error(err.details())
            raise PachydermOperationException()
